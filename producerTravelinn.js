#!/usr/bin/env node
var amqp = require('amqplib/callback_api');

amqp.connect('amqp://rabbitmq:rabbitmq@192.168.99.100:5672', function(err, conn) {
	conn.createChannel(function(err, ch) {
		ch.assertQueue('travelinnQueue', {exclusive: true}, function(err, q) {
			var affiliate = "travelinn";
			console.log('[x] Create reservation(%s)', affiliate);

			ch.consume(q.queue, function(msg) {
				console.log(' ===> Response: %s', msg.content.toString());
				setTimeout(function() { conn.close(); process.exit(0) }, 500);
			}, {noAck: true});

			ch.sendToQueue('reservationQueue', new Buffer(affiliate), { replyTo: q.queue });
		});
	});
});