#!/usr/bin/env node
var amqp = require('amqplib/callback_api');

amqp.connect('amqp://rabbitmq:rabbitmq@192.168.99.100:5672', function(err, conn) {
	conn.createChannel(function(err, ch) {
		var q = 'reservationQueue';
		ch.assertQueue(q, {durable: false});
		ch.prefetch(1);
		console.log(' [x] Afiliates reservation requests');
		ch.consume(q, function reply(msg) {
			var affiliate = msg.content.toString();
			console.log(" [.] create(%s)", affiliate);
			var r = Create(affiliate);
			ch.sendToQueue(msg.properties.replyTo, new Buffer(r.toString()), {});
			ch.ack(msg);
		});
	});
});

function Create(affiliate) {
	var max = 99;
	var min = 1;
	var id = Math.floor(Math.random() * (max - min)) + min;
	return "Reservation ok, id " + id + " on " + affiliate;
}